#cloud-config
users:
  - name: iv_art
    groups: sudo
    shell: /bin/bash
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    ssh-authorized-keys:
      - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCZQpTDbThvmRk7lBnH8buZXaaNPq9/KO4gtUEnt6WNdxA+HUaNxj7JK2zut4Y09kjhWaW1wC9v1G7uvbcLYOxk0t+HoWTrXHN05kjY8u2cvb0oKBqDiJGbKv2cGTBJUTTw4THG/qoYz8+jD7DoynP+BX7iklHOFlyTba+Wi+dm1WFvgHWChdIAYp7c2v6pT2HtsjyHL0qiLnhxE5v4z6/1bTsRWzyw+COtQxzVLwXmm+MLJjh/reGLHPkJl7Lwll1zbWwCqGVjT5QtgAMViBuxaIfG2KU0a3HofDhOulyHRDHfsxqBkaNDJFaZHTLfspDzTzkYAoYRAxyOi9U+F/ebrWlJs1G3ySNUb71LpdH00CdvKI+YdcESrhWC9Tm+Y5Ss6PivfKT0NoZbBGHyn6uykMq2p3bh5kfgXh3RLpRUkOlbtTxKT2JYoixKQi5im/9lmbUFjpg6dBb2shmbbwGeLb5ew/4lcMqH1WwF1MJP2moDISacZUsLTr1Oduj1BpU= iv_art@Pappa"