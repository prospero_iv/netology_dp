// Create SA
resource "yandex_iam_service_account" "my-sa" {
  folder_id = "${var.yandex_folder_id}"
  name      = "terraform-service-account"
}

// Grant permissions
resource "yandex_resourcemanager_folder_iam_binding" "k8s-editor" {
  folder_id = "${var.yandex_folder_id}"
  role      = "editor"
  members = [
    "serviceAccount:${yandex_iam_service_account.my-sa.id}"
  ]
  depends_on = [
    yandex_iam_service_account.my-sa
  ]
}

resource "yandex_resourcemanager_folder_iam_binding" "images-puller" {
// get role  "container-registry.images.puller".
  folder_id = "${var.yandex_folder_id}"
  role      = "container-registry.images.puller"
  members = [
    "serviceAccount:${yandex_iam_service_account.my-sa.id}"
  ]
  depends_on = [
    yandex_iam_service_account.my-sa
  ]

}

