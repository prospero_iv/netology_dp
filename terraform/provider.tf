# Provider
terraform {

 cloud {
  organization = "example-org-ebb0ba"
    workspaces {
      name = "stage"
    }
  }

  required_version = ">= 1.2.7"

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.77.0"
    }
  }
}

provider "yandex" {
  cloud_id  = "${var.yandex_cloud_id}"
  folder_id = "${var.yandex_folder_id}"
  token = "${var.token}"
}
