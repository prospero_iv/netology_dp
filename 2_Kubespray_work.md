#### Подготовка  
 
Аналогично домашнего задания 12-4 по созданию кластера:  
- Cклонировал себе репозиторий:  

git clone https://github.com/kubernetes-sigs/kubespray  

- Установил зависимости  
sudo pip3 install -r requirements.txt  

- Копировал папку с конфигурацией  
cp -rfp inventory/sample inventory/cluster  

- Переключил Terraform Cloud в режим local и получил готовый файл hosts.yaml  

```  
---
all:
  hosts:
    control-plane:
      ansible_host: 178.154.223.212
      ansible_user: iv_art
    node-1:
      ansible_host: 158.160.15.172
      ansible_user: iv_art
    node-2:
      ansible_host: 51.250.32.244
      ansible_user: iv_art
    node-3:
      ansible_host: 51.250.71.174
      ansible_user: iv_art
  children:
    kube_control_plane:
      hosts:
        control-plane:
    kube_node:
      hosts:
        node-1:
        node-2:
        node-3:
    etcd:
      hosts:
        control-plane:
    k8s_cluster:
      vars:
        supplementary_addresses_in_ssl_keys: [178.154.223.212]
      children:
        kube_control_plane:
        kube_node:
    calico_rr:
      hosts: {}
```  

#### Запуск   

ansible-playbook -i inventory/mycluster/hosts.yaml cluster.yml -b -v  
![cluster](kubespray/images/cluster.jpg)  

Результат  
![result](kubespray/images/result.jpg)  

или 
```
iv_art@Pappa-wsl:~/kubernetes-for-beginners-master/99-misc$ ./list-vms.sh
+----------------------+---------------------------+---------------+---------+-----------------+---------------+
|          ID          |           NAME            |    ZONE ID    | STATUS  |   EXTERNAL IP   |  INTERNAL IP  |
+----------------------+---------------------------+---------------+---------+-----------------+---------------+
| ef39a637c4291ik38de9 | cl1d7fqk36a2sa35bg37-eqoz | ru-central1-c | RUNNING | 51.250.32.244   | 192.168.30.34 |
| epdq3odt3v7a3oej0d41 | cl1d7fqk36a2sa35bg37-egeb | ru-central1-b | RUNNING | 158.160.15.172  | 192.168.20.9  |
| fhma9c9v5vh1fkpipju4 | control-plane             | ru-central1-a | RUNNING | 178.154.223.212 | 192.168.10.5  |
| fhmoek2sid2qvpi816k2 | cl1d7fqk36a2sa35bg37-ocah | ru-central1-a | RUNNING | 51.250.71.174   | 192.168.10.23 |
+----------------------+---------------------------+---------------+---------+-----------------+---------------+
```
Кластер готов  
