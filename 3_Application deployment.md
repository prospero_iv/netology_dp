#### Создание тестового приложения  

В качестве образа взял просто nginx и немного поменял страницу, добавив картинку    
```
iv_art@Pappa-wsl:~/diplomus/app$ cat index.html
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to my test nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<img src="primer.png">

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```
Dockerfile  
```
iv_art@Pappa-wsl:~/diplomus/app$ cat Dockerfile
FROM ubuntu/nginx

COPY ./index.html var/www/html/index.nginx-debian.html
COPY ./primer.png var/www/html/primer.png


EXPOSE 80
```
и deploy с сервисом  
```
iv_art@Pappa-wsl:~/diplomus/app$ cat deployment.yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: test-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: test-app
  template:
    metadata:
      labels:
        app: test-app
    spec:
      containers:
        - name: test-app
          image: ivart074/test_app
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
iv_art@Pappa-wsl:~/diplomus/app$ cat service.yaml
---
apiVersion: v1
kind: Service
metadata:
  name: test-app-svc
spec:
  type: NodePort
  selector:
    app: test-app
  ports:
    - name: test-app
      nodePort: 30900
      port: 80
      targetPort: 80
```
Применяем  
```
iv_art@Pappa-wsl:~/diplomus/app$ kubectl apply -f deployment.yaml
deployment.apps/test-app created
iv_art@Pappa-wsl:~/diplomus/app$ kubectl apply -f service.yaml
service/test-app-svc created

iv_art@Pappa-wsl:~/diplomus/app$ kubectl get po,svc
NAME                            READY   STATUS    RESTARTS      AGE
pod/test-app-56f6d5b54b-g8k57   1/1     Running   2 (18h ago)   22h

NAME                   TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
service/kubernetes     ClusterIP   10.233.0.1      <none>        443/TCP        2d23h
service/test-app-svc   NodePort    10.233.26.236   <none>        80:30900/TCP   2m55s
```

Итого  
![screen_app](app/images/screen_app.jpg)  

Репо приложения здесь  
https://gitlab.com/prospero_iv/dp_test_app   



