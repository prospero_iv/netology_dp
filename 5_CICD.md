#### Установка и настройка CI/CD  

CI/CD процесс организовал в Gitlab  
Ссылка на репозиторий: https://gitlab.com/prospero_iv/dp_test_app  

1. Создадим файл конфига .gitlab/agents/gitlab-agent/config.yaml, в котором пропишем путь к проекту, к которому должен быть открыт доступ агенту.  

2. разворачиваем агента:  
```
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install gitlab-agent gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set image.tag=v15.6.0-rc1 \
    --set config.token=LK1HfBJRvKEKJDi........zAkJszzjTWA \
    --set config.kasAddress=wss://kas.gitlab.com
```

3. Устанавливаем и регистрируем раннер в Kubernetes (инструкция отсюда https://docs.gitlab.com/runner/install/kubernetes.html )

```
iv_art@Pappa-wsl:~/test_app$ helm install --namespace gitlab-agent gitlab-runner -f values.yaml gitlab/gitlab-runner
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/iv_art/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/iv_art/.kube/config
NAME: gitlab-runner
LAST DEPLOYED: Thu Nov  3 23:36:17 2022
NAMESPACE: gitlab-agent
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Your GitLab Runner should now be registered against the GitLab instance reachable at: "https://gitlab.com/"

Runner namespace "gitlab-agent" was found in runners.config template.
```
4. Создаем .gitlab-ci.yml с конфигурацией на kaniko согласно инструкции  

https://docs.gitlab.com/ee/ci/docker/using_kaniko.html  

```
---
stages:
  - build
  - deploy


build_latest_commit:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:v1.9.0-debug
    entrypoint: [""]
  script:
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}" 
  rules:
    - if: !$CI_COMMIT_TAG 

build_tag_commit:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:v1.9.0-debug
    entrypoint: [""]
  script:
    - echo "Run build with tag!!!$CI_COMMIT_TAG!!!"
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}" 
  rules:
    - if: $CI_COMMIT_TAG 

deploy_app:
  stage: deploy
  image:
    name: dtzar/helm-kubectl:latest
    entrypoint: [""]
  before_script:
    - kubectl config use-context prospero_iv/dp_test_app:gitlab-agent
  script:
    - helm upgrade test-app ./deploy --install --set-string image.tag=${CI_COMMIT_TAG}
  rules:
    - if: $CI_COMMIT_TAG 

```
5. Запускаем коммит без тэга  
проходит только сборка и размещение в регистр  
https://gitlab.com/prospero_iv/dp_test_app/-/pipelines/685384983  
![commit](cicd/images/commit.jpg)  
6. Запускаем с тэгом  
проходят все этапы CI/CD  
https://gitlab.com/prospero_iv/dp_test_app/-/pipelines/685389328  
![git_tag](cicd/images/git_tag.jpg)  
  
![git_tag_pipe](cicd/images/git_tag_pipe.jpg)  
Регистр  образов  
![registry](cicd/images/registry.jpg)  

7. Сборка и деплой успешны, проверяем поды  
![git_tag](cicd/images/pods.jpg)  
```
iv_art@Pappa-wsl:~/diplomus/cicd/images$ kubectl describe po test-app-75677f6c89-qm6nk -n gitlab-agent
Name:         test-app-75677f6c89-qm6nk
Namespace:    gitlab-agent
Priority:     0
Node:         node-2/192.168.30.34
Start Time:   Fri, 04 Nov 2022 15:51:38 +1000
Labels:       app=test-app
              pod-template-hash=75677f6c89
Annotations:  cni.projectcalico.org/containerID: 1ed882e8543c94eef62c9a13b9398cdba38a9114edd024fe0209f12a99746de5
              cni.projectcalico.org/podIP: 10.233.65.108/32
              cni.projectcalico.org/podIPs: 10.233.65.108/32
Status:       Running
IP:           10.233.65.108
IPs:
  IP:           10.233.65.108
Controlled By:  ReplicaSet/test-app-75677f6c89
Containers:
  test-app:
    Container ID:   containerd://e6b28c26ae57fb80b98d4dbf11ceca4f709eb1061f49097136e823c6a65212d8
    Image:          registry.gitlab.com/prospero_iv/dp_test_app:2.1.0
    Image ID:       registry.gitlab.com/prospero_iv/dp_test_app@sha256:ef07911b966cfbf0370a19dbffd26293ae8006473fa85961ac8c83c3aba5996d
    Port:           80/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Fri, 04 Nov 2022 15:51:43 +1000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-2t55z (ro)
Conditions:
  Type              Status
  Initialized       True
  Ready             True
  ContainersReady   True
  PodScheduled      True
Volumes:
  kube-api-access-2t55z:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  10m   default-scheduler  Successfully assigned gitlab-agent/test-app-75677f6c89-qm6nk to node-2
  Normal  Pulling    10m   kubelet            Pulling image "registry.gitlab.com/prospero_iv/dp_test_app:2.1.0"
  Normal  Pulled     10m   kubelet            Successfully pulled image "registry.gitlab.com/prospero_iv/dp_test_app:2.1.0" in 3.390002639s
  Normal  Created    10m   kubelet            Created container test-app
  Normal  Started    10m   kubelet            Started container test-app
iv_art@Pappa-wsl:~/diplomus/cicd/images$
```
Проект работает  )  


