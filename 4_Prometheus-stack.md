#### Подготовка системы мониторинга и деплой приложения

Для установки пакета воспользовался статьей https://itsecforu.ru/2021/04/12/%E2%98%B8%EF%B8%8F-%D0%BA%D0%B0%D0%BA-%D1%83%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%82%D1%8C-prometheus-%D0%B8-grafana-%D0%BD%D0%B0-kubernetes-%D1%81-%D0%BF%D0%BE%D0%BC%D0%BE%D1%89%D1%8C%D1%8E-h/ 

```
iv_art@MININT-546221:~/.kube$ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/iv_art/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/iv_art/.kube/config
"prometheus-community" has been added to your repositories

iv_art@MININT-546221:~/.kube$ helm repo update
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/iv_art/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/iv_art/.kube/config
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "prometheus-community" chart repository
Update Complete. ⎈Happy Helming!⎈

iv_art@MININT-546221:~/.kube$ helm install prometheus-stack prometheus-community/kube-prometheus-stack
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/iv_art/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/iv_art/.kube/config
NAME: prometheus-stack
LAST DEPLOYED: Sun Oct 30 22:25:23 2022
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
kube-prometheus-stack has been installed. Check its status by running:
  kubectl --namespace default get pods -l "release=prometheus-stack"
```
  Для доступа извне создаем сервис  
```
---
apiVersion: v1
kind: Service
metadata:
  name: prometheus
spec:
  type: NodePort
  selector:
    app.kubernetes.io/name: grafana
  ports:
    - name: http
      nodePort: 30902
      port: 3000
      targetPort: 3000
```

Получаем сформированный стек  
```
iv_art@MININT-546221:~/diplomus/monitoring$ kubectl get po,svc -o wide
NAME                                                         READY   STATUS    RESTARTS      AGE   IP              NODE            NOMINATED NODE   READINESS GATES
pod/alertmanager-prometheus-stack-kube-prom-alertmanager-0   2/2     Running   1 (15m ago)   15m   10.233.75.70    node-3          <none>           <none>
pod/prometheus-prometheus-stack-kube-prom-prometheus-0       2/2     Running   0             15m   10.233.70.69    node-1          <none>           <none>
pod/prometheus-stack-grafana-b66b97d4b-75psh                 3/3     Running   0             15m   10.233.65.69    node-2          <none>           <none>
pod/prometheus-stack-kube-prom-operator-64474c6d75-vd6xv     1/1     Running   0             15m   10.233.70.68    node-1          <none>           <none>
pod/prometheus-stack-kube-state-metrics-54b5bc9fdb-m6t5z     1/1     Running   0             15m   10.233.70.67    node-1          <none>           <none>
pod/prometheus-stack-prometheus-node-exporter-9z2wm          1/1     Running   0             15m   192.168.10.23   node-3          <none>           <none>
pod/prometheus-stack-prometheus-node-exporter-g5bbn          1/1     Running   0             15m   192.168.30.34   node-2          <none>           <none>
pod/prometheus-stack-prometheus-node-exporter-hjx4x          1/1     Running   0             15m   192.168.10.5    control-plane   <none>           <none>
pod/prometheus-stack-prometheus-node-exporter-pf29l          1/1     Running   0             15m   192.168.20.9    node-1          <none>           <none>
pod/test-app-56f6d5b54b-g8k57                                1/1     Running   2 (20h ago)   24h   10.233.75.69    node-3          <none>           <none>

NAME                                                TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE    SELECTOR
service/alertmanager-operated                       ClusterIP   None            <none>        9093/TCP,9094/TCP,9094/UDP   15m    app.kubernetes.io/name=alertmanager
service/kubernetes                                  ClusterIP   10.233.0.1      <none>        443/TCP                      3d     <none>
service/prometheus                                  NodePort    10.233.63.50    <none>        3000:31000/TCP               17s    app.kubernetes.io/name=grafana
service/prometheus-operated                         ClusterIP   None            <none>        9090/TCP                     15m    app.kubernetes.io/name=prometheus
service/prometheus-stack-grafana                    ClusterIP   10.233.39.167   <none>        80/TCP                       15m    app.kubernetes.io/instance=prometheus-stack,app.kubernetes.io/name=grafana
service/prometheus-stack-kube-prom-alertmanager     ClusterIP   10.233.44.131   <none>        9093/TCP                     15m    alertmanager=prometheus-stack-kube-prom-alertmanager,app.kubernetes.io/name=alertmanager
service/prometheus-stack-kube-prom-operator         ClusterIP   10.233.47.119   <none>        443/TCP                      15m    app=kube-prometheus-stack-operator,release=prometheus-stack
service/prometheus-stack-kube-prom-prometheus       ClusterIP   10.233.37.19    <none>        9090/TCP                     15m    app.kubernetes.io/name=prometheus,prometheus=prometheus-stack-kube-prom-prometheus
service/prometheus-stack-kube-state-metrics         ClusterIP   10.233.60.245   <none>        8080/TCP                     15m    app.kubernetes.io/instance=prometheus-stack,app.kubernetes.io/name=kube-state-metrics
service/prometheus-stack-prometheus-node-exporter   ClusterIP   10.233.30.110   <none>        9100/TCP                     15m    app.kubernetes.io/instance=prometheus-stack,app.kubernetes.io/name=prometheus-node-exporter
service/test-app-svc                                NodePort    10.233.26.236   <none>        80:30902/TCP                 107m   app=test-app
```
Заходим, проверяем  
![grafana](monitoring/images/grafana.png)
