Дипломное задание доступно по ссылке:  
https://github.com/netology-code/devops-diplom-yandexcloud  

Все этапы описаны в соответствующих файлах в корневой папке  
Как результат:
Тестовое приложение деплоится и работает:  
http://178.154.223.212:30900/  
  
![result1](result1.jpg)  

Prometheus+Grafana стек также функционируют  
http://178.154.223.212:30902/  

Логин admin  
Пароль prom-operator  

![prom](prom.jpg)  
