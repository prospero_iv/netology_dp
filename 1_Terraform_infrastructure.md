#### Создание инфраструктуры.  

В качестве бекенда для Терраформа выбран Terraform Cloud.  
В Terraform Cloud создан workspace stage. Тип подключения - CLI с работой в local варианте для хранения в TC только состояний.   
В блок provider добавлены параметры облака  

```
# Provider
terraform {

 cloud {
  organization = "example-org-ebb0ba"
    workspaces {
      name = "stage"
    }
  }
```
##### Стартуем конфигурацию  

![start](terraform/images/start.jpg)

![cloud](terraform/images/cloud.jpg)

##### Полученная структура

![k8group](terraform/images/k8group.jpg)
![vm](terraform/images/vms.jpg)
![net](terraform/images/vpc.jpg)

Теперь cтейты и рабочие файлы полученной инфраструктуры могут храниться в Terraform Cloud. 

Но для удобства переключил в режим Local, чтобы hosts.yml создался в будущей структуре kubespray


